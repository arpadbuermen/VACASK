#ifndef __DEVBUILTINS_DEFINED
#define __DEVBUILTINS_DEFINED

#include "devbase.h"
#include "common.h"

namespace NAMESPACE {

void createBuiltins(std::vector<Device*>& devices); 

}

#endif

